/*
Matlab applications in physics
Author: Adam Stasiak
Engineering Physics
*/

//function for calculating the spring constant
function k =  SpringConstant(shearModulus, wireRadius, numOfSpringTurns, springCoilRadius)
    k = (shearModulus*wireRadius^4)/(4*numOfSpringTurns*springCoilRadius^3);
endfunction

//function for calculating the amplitude
function y = Amplitude(alpha0, undampedAngularFrequency, angularFrequency, dampingFactor)
    y = (alpha0/sqrt( (undampedAngularFrequency^2 - angularFrequency^2)^2 + 4*dampingFactor^2*angularFrequency^2));
endfunction

path = get_absolute_file_path('spring.sce'); //path to the folder where this script is located
filename = 'resonance_analysis_results.dat'; //name of the data file

//constants
shearModulus = 79.3e9; //source: https://en.wikipedia.org/wiki/Shear_modulus
wireRadius = 2.0e-3;
springCoilRadius = 0.0254*2.0;
g = 9.81; //standard gravity
mass = 10e-3;
dampingTimeConstant = 2.0;
dampingFactor = 1.0/(2.0*dampingTimeConstant);

numOfSpringTurns = 2:50; //spring coil range
angularFrequency = 50:0.05:400; //angular frequency range

//calculation of resonance curve
for i = 1:size(numOfSpringTurns)(2)
   k = SpringConstant(shearModulus, wireRadius, numOfSpringTurns(i), springCoilRadius); //calculation of spring constant
   amplitude(i, 1) = Amplitude(g, sqrt(k/mass), angularFrequency(1), dampingFactor); //calculation of amplitude for the first angular frequency
   for j = 2:size(angularFrequency)(2)
      amplitude(i, j) = Amplitude(g, sqrt(k/mass), angularFrequency(j), dampingFactor); //calculation of amplitude for the j-th angular frequency
      if amplitude(i, j) > amplitude(i, j-1)
          resonanceAngularFrequency(i) = angularFrequency(j); //finding the resonance angular frequency for the i-th number of spring turns
      end
   end
   resonanceAmplitude(i) = max(amplitude(i, :)); //calculation of resonance amplitude for the i-th number of spring turns
end

//drawing a chart of resonance curves
subplot(211);
plot(angularFrequency, amplitude(1:size(numOfSpringTurns)(2), :));
xtitle( 'A = f(ω) dla różnej liczby zwojów sprężyny N' , 'Częstość pobudzania układu ω, Hz', 'Amplituda A, m');
//setting axes scale
axes = gca();
axes.data_bounds = [50, 0 ; 360, 0.3];
//drawing a chart of the resonance amplitude versus the number of spring turns
subplot(212);
plot(numOfSpringTurns, resonanceAmplitude);
xtitle( 'A_m = f(N)' , 'Liczba zwojów sprężyny N', ' Amplituda rezonansowa A_m, m');
//setting axes scale
axes = gca();
axes.data_bounds = [0, 0 ; 50, 0.3];

xs2pdf(0, 'resonance.pdf'); //saving charts in pdf-file

outputFile = mopen(path + filename, 'wt'); //opening the file

//writing information about used constants to a file
mfprintf(outputFile, '%s %.2f %s; %s\n', 'Moduł sztywności materiału G =' , shearModulus * 10^-9, 'GPa', 'źródło: https://en.wikipedia.org/wiki/Shear_modulus');
mfprintf(outputFile, '%s %.1f %s\n', 'Promień drutu sprężyny r =' , wireRadius*100, 'mm');
mfprintf(outputFile, '%s %.2f %s\n', 'Promień zwoju sprężyny R =' , springCoilRadius*100, 'mm');
mfprintf(outputFile, '%s %.2f %s\n', 'g =' , g, 'm/s^2');
mfprintf(outputFile, '%s %.1f %s\n', 'm =' , mass*1000, 'g');
mfprintf(outputFile, '%s %.1f %s\n', 'stała czasowa tłumienia =' , dampingTimeConstant, 's');

//writing a table header to a file
mfprintf(outputFile, '\n');
mfprintf(outputFile, "%s%2s%3s%25s%2s%25s%2s\n\n", '|', 'N', '|', 'Amplituda rezonansowa, m', '|', 'Częstość rezonansowa, Hz', '|');

//writing a data to table located in a file
for i = 1:size(numOfSpringTurns)(2)
    mfprintf(outputFile, "%s%3.0f%2s%17f%10s%16.1f%11s\n", '|', numOfSpringTurns(i), '|', resonanceAmplitude(i), '|', resonanceAngularFrequency(i), '|') ;
end

mclose(outputFile); //closing the file

clear; //clearing memory
